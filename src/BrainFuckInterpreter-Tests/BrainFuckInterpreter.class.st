Class {
	#name : #BrainFuckInterpreter,
	#superclass : #TestCase,
	#category : #'BrainFuckInterpreter-Tests'
}

{ #category : #tests }
BrainFuckInterpreter >> testCodeStream [
	|bf str |
	str := '+[----->+++<]>+.+.'.
	bf := BrainFuck code: str.
	self assert: bf codeStream equals: str

]

{ #category : #tests }
BrainFuckInterpreter >> testDecrement [
	|bf|
	bf := BrainFuck new.
	bf increment.
	bf increment.
	bf decrement.
	self assert: (bf memory at: bf stackPointer) equals: 1.
]

{ #category : #tests }
BrainFuckInterpreter >> testDecrementPointer [
	|bf|
	bf := BrainFuck new.
	bf decrementPointer.
	self assert: bf stackPointer equals: 0.
]

{ #category : #tests }
BrainFuckInterpreter >> testDecrementRollOver [
	|bf|
	bf := BrainFuck new.
	bf increment.
	bf decrement.
	bf decrement.
	self assert: (bf memory at: bf stackPointer) equals: 255.
]

{ #category : #tests }
BrainFuckInterpreter >> testIncrement [
	|bf|
	bf := BrainFuck new.
	bf increment.
	self assert: (bf memory at: bf stackPointer) equals: 1.
]

{ #category : #tests }
BrainFuckInterpreter >> testIncrementMemoryIncrease [
	|bf arr|
	bf := BrainFuck new.
	bf stackPointer: bf stackPointer + 1.
	bf incrementPointer.
	arr := { 0 . 0  }.
	self assert: bf memory equals: arr.
]

{ #category : #tests }
BrainFuckInterpreter >> testIncrementPointer [
	|bf|
	bf := BrainFuck new.
	bf incrementPointer.
	self assert: bf stackPointer equals: 2.
]

{ #category : #tests }
BrainFuckInterpreter >> testIncrementRollOver [
	|bf|
	bf := BrainFuck new.
	1 to: 256 do: [ :i | bf increment.].
	self assert: (bf memory at: bf stackPointer) equals: 0.
]

{ #category : #tests }
BrainFuckInterpreter >> testInitialization [
	|bf arr|
	bf := BrainFuck new.
	arr := Array with: 0.
	self assert: bf codePointer equals: 1.
	self assert: bf stackPointer equals: 1.
	self assert: bf memory equals: arr.
	self assert: bf loop equals: Dictionary new.
]

{ #category : #tests }
BrainFuckInterpreter >> testLoops [
	|bf str collect|
	str := '+[----->+++<]>+.+.'.
	bf := BrainFuck code: str.
	collect := Dictionary new.
	collect at: 2 put: 13.
	collect at: 13 put: 2.
	self assert: bf loop equals: collect.

]
