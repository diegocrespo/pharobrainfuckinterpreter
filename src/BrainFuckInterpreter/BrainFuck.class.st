"
I am a BrainFuck Interpreter. I will run BrainFuck code when given a string

""BrainFuck code: aString"".
"
Class {
	#name : #BrainFuck,
	#superclass : #Object,
	#instVars : [
		'memory',
		'stackPointer',
		'codePointer',
		'loop',
		'codeStream'
	],
	#category : #BrainFuckInterpreter
}

{ #category : #accessing }
BrainFuck class >> code: aString [
	^ (self new codeStream: aString) putLoop.
]

{ #category : #initialization }
BrainFuck >> asciiCharacter [
	Transcript show: (Character value: (self memory at: stackPointer))
]

{ #category : #initialization }
BrainFuck >> codePointer [
	^ codePointer
]

{ #category : #initialization }
BrainFuck >> codePointer: aNumber [
	codePointer := aNumber
]

{ #category : #accessing }
BrainFuck >> codeStream [
	^ codeStream
]

{ #category : #accessing }
BrainFuck >> codeStream: aString [
	codeStream := aString
]

{ #category : #initialization }
BrainFuck >> decrement [
	| val |
	val := (memory at: stackPointer).
	val > 0 ifTrue: [ memory at: stackPointer put: val - 1 ]
	ifFalse: [ memory at: stackPointer put: 255 ]
]

{ #category : #initialization }
BrainFuck >> decrementPointer [
	stackPointer := stackPointer - 1.
]

{ #category : #initialization }
BrainFuck >> increment [
	| val |
	memory size < stackPointer ifTrue:[ memory copyWith: 0].
	val := (memory at: stackPointer).
	val < 255 ifTrue: [ memory at: stackPointer put: val + 1 ]
	ifFalse: [ memory at: stackPointer put: 0 ]
]

{ #category : #initialization }
BrainFuck >> incrementPointer [
	stackPointer := stackPointer + 1.
	memory size < stackPointer ifTrue:[ memory := memory copyWith: 0].
]

{ #category : #initialization }
BrainFuck >> initialize [
	memory := {0}.
	self stackPointer: 1.
	self codePointer: 1.
	loop := Dictionary new.
]

{ #category : #initialization }
BrainFuck >> iterate [
	| code len char|
	code := self codeStream.
	len := code size.
	
	[self codePointer < (len + 1)] whileTrue: [
		char := code at: codePointer.
		char = $+ ifTrue: [ self increment ].
		char = $- ifTrue: [ self decrement ].
		char = $> ifTrue: [ self incrementPointer ].
		char = $< ifTrue: [ self decrementPointer ].
		char = $[ ifTrue: [ 
			(memory at: stackPointer) = 0 ifTrue: [ 
				codePointer := loop at: codePointer ] ].
		char = $] ifTrue: [ 
			(memory at: stackPointer) ~= 0 ifTrue: [ 
				codePointer := loop at: codePointer ] ].
		char = $. ifTrue: [ 
			Transcript show: (Character value: (memory at: stackPointer)) ].
		char = $, .
		codePointer := codePointer + 1].

]

{ #category : #initialization }
BrainFuck >> loop [
	^ loop
]

{ #category : #initialization }
BrainFuck >> memory [
	^ memory 
]

{ #category : #initialization }
BrainFuck >> putLoop [
	"Creates the collection of where the loops start and end in the script"
	|start|
	self codeStream withIndexDo: [ :char :index |
		char = $[ ifTrue: [ start := index ].
		char = $] ifTrue: [ 
		loop at: start put: index.
		loop at: index put: start.
"		loop at: start put: index.
		loop at: index put: start" ].
		 ].
]

{ #category : #initialization }
BrainFuck >> stackPointer [
	^ stackPointer
]

{ #category : #initialization }
BrainFuck >> stackPointer: aNumber [
	stackPointer := aNumber
]
